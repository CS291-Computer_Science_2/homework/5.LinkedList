#ifndef TEST_H
#define TEST_H
#include <iostream>
using namespace std;

struct student {
	string name;
	int id;
	float gpa;

	student *next;
};

class List : public student {
	public:
		List();
		~List();
		void append(string, int, float);
		void print();
	private:
		student *head;
};

#endif
