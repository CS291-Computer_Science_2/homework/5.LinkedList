#include <iostream>
#include <string>
#include "list.h"
using namespace std;

List::List() : head(NULL) {	//initialize student head

}

List::~List() {

}

void List::print(){
	student *temp = head;
	while(temp){
		cout<<temp->name<<" "<<temp->id<<" "<<temp->gpa<<endl;
		
		temp=temp->next;
	}
}

void List::append(string name, int id, float gpa) {
	student *newptr = new student;	//new struct
	newptr->name= name;
	newptr->id= id;
	newptr->gpa= gpa;
	newptr->next= NULL;

	if (head == NULL) head= newptr;
	else {
		student *temp= head;
		while(temp->next) temp= temp->next; //MOVE THE ADDRESS
		temp->next= newptr; //RECURSE
	}
}
